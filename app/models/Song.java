package models;

import javax.persistence.*;
import models.*;

/**
 * Clase que modela la tabla song de la base de datos (representa una Cancion)
 */
@Entity
public class Song{

	/**
	 * Atributo que modela el titulo de la cancion
	 */
	@Column(name="title")
	private String title;

	/**
	 * Atributo que modela la direccion url de la cancion
	 */
	@Id
	@Column(name="uri", unique=true)
	private String uri;

	/**
	 * Atributo que representa el artista (del cual se obtiene el username)
	 */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="username_artist")
	private Artist artist;

	/**
	 * Metodo que instancia una Cancion vacio
	 */
	public Song (){

	}

	/**
	 * MEtodo que instancia una Cancion con todos sus campos requeridos
	 * @param title Requiere el Titulo de la cancion
	 * @param uri Requiere la URL donde se aloja la cancion
	 * @param artist Requiere el artista que sube la cancion
	 */
	public Song (String title, String uri, Artist artist){
		this.title = title;
		this.uri = uri;
		this.artist = artist;
	}

	/**
	 * MEtodo que modifica al artista que sube la cancion
	 * @param artist el artista por el caul se desea modificar
	 */
	public void setArtist(Artist artist){
		this.artist = artist;
	}

	/**
	 * MEtodo que obtiene al Artista
	 * @return El artista que sube la cancion
	 */
	public Artist getArtist(){
		return artist;
	}

	/**
	 * MEtodo que modifica el titulo de la cancion
	 * @param title El titulo de la cancion por el caul modicara el actual
	 */
	public void setTitle(String title){
		this.title = title;
	}

	/**
	 * MEtodo que obtiene el titulo de la cancion
	 * @return El titulo de la cancion
	 */
	public String getTitle(){
		return title;
	}

	/**
	 * MEtodo que modifica la URL de la cancion
	 * @param uri url por la cual se desea cambiar el actual
	 */
	public void setUri(String uri){
		this.uri = uri;
	}

	/**
	 * Metodo que obtiene la direccion URL de la cancion
	 * @return url donde se encuentra alojada la cancion
	 */
	public String getUri(){
		return uri;
	}

	@Override
	public String toString(){
		return "Song: title:" + title + " in:" + uri + " by:" + artist.getName();
	}

}
