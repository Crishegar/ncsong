package models;

import javax.persistence.*;
import models.Song;
import java.util.List;
import play.data.validation.Constraints;

/**
 * Clase que modela la Tabla artist de la base de datos
 */
@Entity
@Table(name = "artist",
		uniqueConstraints={@UniqueConstraint(columnNames={"username"})})
public class Artist{

	/**
	 * Atributo que modela el campo username (tambien es la primary key) del artista
	 */
	@Id
	@Column(name="username")
	private String username;

	/**
	 * Atributo que modela el campo pass (contraseña) del artista
	 */
	@Column(name="pass")
	private String pass;

	/**
	 * Atributo que modela el campo name (nombres) del artista
	 */
	@Column(name="name")
	private String name;

	/**
	 * Atributo que modela el campo lastname (apellidos) del artista
	 */
	@Column(name="lastname")
	private String lastname;

	/**
	 * Atributo que establece la relacion uno a muchos con la tabla song
	 * Y contiene todas las canciones de un artista.
	 */
	@OneToMany(mappedBy="artist")
	private List <Song> song;

	/**
	 * Metodo que instancia un Artista, sin campos
	 */
	public Artist(){

	}

	/**
	 * MEtodo que instancia a un ARtista con todos sus datos.
	 * @param username Requiere el username de la cuenta
	 * @param pass Requiere la contraseña de la cuenta
	 * @param name Requiere el nombre del artista
	 * @param lastname Requiere el apellido del artista
	 */
	public Artist(String username, String pass, String name, String lastname){
		this.username = username;
		this.pass = pass;
		this.name = name;
		this.lastname = lastname;
	}

	/**
	 * Metodo que obtiene el username de la cuenta del artista
	 * @return retorna el username
	 */
	public String getUsername(){
		return username;
	}

	/**
	 * Metodo que modifica el username del usuario
	 * @param username el username por el cual se desea modificar el actual
	 */
	public void setUsername(String username){
		this.username = username;
	}

	/**
	 * Metodo que obtiene la contraseña de la cuenta del artista
	 * @return retorna la contraseña
	 */
	public String getPass(){
		return pass;
	}

	/**
	 * Metodo que modifica la contraseña de la cuenta del artista
	 * @param pass Contraseña por la cual se desea modificar la actual
	 */
	public void setPass(String pass){
		this.pass = pass;
	}

	/**
	 * Metodo que obtiene el nombre de la cuenta del artista
	 * @return retorna el nombre
	 */
	public String getName(){
		return name;
	}

	/**
	 * Metodo que modifica el nombre del artista
	 * @param name nombre por el cual se desea modificar el actual
	 */
	public void setName(String name){
		this.name = name;
	}

	/**
	 * Metodo que obtiene el apellido de la cuenta del artista
	 * @return retorna el apellido
	 */
	public String getLastname(){
		return lastname;
	}

	/**
	 * MEtodo que modifica el apellido del artista
	 * @param lastname apellido por el cual se desea modificar el actual
	 */
	public void setLastname(String lastname){
		this.lastname = lastname;
	}

	/**
	 * Metodo que obtiene la lista de canciones del artista
	 * @return la lista de canciones
	 */
	public List<Song> getSongs(){
		return song;
	}

	@Override
	public String toString(){
		return "Artista: name=" + name + " lastname=" + lastname;  
	}
}