name := """ncsong"""
organization := "cristian.org"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.2"

libraryDependencies += guice


herokuAppName in Compile := "ncsong"


herokuConfigVars in Compile := Map(
    "JDBC_DATABASE_URL" -> "jdbc:postgresql://ec2-23-23-220-163.compute-1.amazonaws.com:5432/d1jptpb6fv3fko?user=gyeahxlfhywbfr&password=06c6433b6ffb72004bc2674a18cf33cd51bbeea925fe2e467bde34c52a4a0b27&sslmode=require"
)

libraryDependencies ++= Seq(
  javaJdbc,
  evolutions,
  jdbc,
  javaJpa,
  "org.hibernate" % "hibernate-entitymanager" % "5.1.0.Final",
  "org.postgresql" % "postgresql" % "9.4-1206-jdbc42",
  "com.cloudinary" % "cloudinary-http44" % "1.13.0"
)

PlayKeys.externalizeResources := false
