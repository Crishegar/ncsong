
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/Asus/Documents/Cristian/Tesis/ncsong/conf/routes
// @DATE:Mon Jul 17 10:11:14 COT 2017

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  Assets_0: controllers.Assets,
  // @LINE:9
  SongController_2: controllers.SongController,
  // @LINE:12
  ArtistController_1: controllers.ArtistController,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    Assets_0: controllers.Assets,
    // @LINE:9
    SongController_2: controllers.SongController,
    // @LINE:12
    ArtistController_1: controllers.ArtistController
  ) = this(errorHandler, Assets_0, SongController_2, ArtistController_1, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, Assets_0, SongController_2, ArtistController_1, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    ("""GET""", this.prefix, """controllers.SongController.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """signin""", """controllers.ArtistController.create"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """signin""", """controllers.ArtistController.addArtist"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logs/""" + "$" + """username<[^/]+>""", """controllers.ArtistController.show(username:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logs/""" + "$" + """username<[^/]+>""", """controllers.ArtistController.delete(username:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logs/""" + "$" + """username<[^/]+>/edit""", """controllers.ArtistController.edit(username:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logs/""" + "$" + """username<[^/]+>/edit""", """controllers.ArtistController.update(username:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """log""", """controllers.ArtistController.search"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """log""", """controllers.ArtistController.buscar"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logs/""" + "$" + """username<[^/]+>/uploadSong""", """controllers.ArtistController.uploadSong(username:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logs/""" + "$" + """username<[^/]+>/uploadSong""", """controllers.SongController.upload(username:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logs/""" + "$" + """username<[^/]+>/""" + "$" + """uri<[^/]+>/d""", """controllers.SongController.delete(uri:String, username:String)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_Assets_versioned0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned0_invoker = createInvoker(
    Assets_0.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )

  // @LINE:9
  private[this] lazy val controllers_SongController_index1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_SongController_index1_invoker = createInvoker(
    SongController_2.index,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SongController",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """ Controller showing a sample home page""",
      Seq()
    )
  )

  // @LINE:12
  private[this] lazy val controllers_ArtistController_create2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("signin")))
  )
  private[this] lazy val controllers_ArtistController_create2_invoker = createInvoker(
    ArtistController_1.create,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ArtistController",
      "create",
      Nil,
      "GET",
      this.prefix + """signin""",
      """Create Artist Account""",
      Seq()
    )
  )

  // @LINE:13
  private[this] lazy val controllers_ArtistController_addArtist3_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("signin")))
  )
  private[this] lazy val controllers_ArtistController_addArtist3_invoker = createInvoker(
    ArtistController_1.addArtist,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ArtistController",
      "addArtist",
      Nil,
      "POST",
      this.prefix + """signin""",
      """""",
      Seq()
    )
  )

  // @LINE:16
  private[this] lazy val controllers_ArtistController_show4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logs/"), DynamicPart("username", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ArtistController_show4_invoker = createInvoker(
    ArtistController_1.show(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ArtistController",
      "show",
      Seq(classOf[String]),
      "GET",
      this.prefix + """logs/""" + "$" + """username<[^/]+>""",
      """Show Artist Account""",
      Seq()
    )
  )

  // @LINE:19
  private[this] lazy val controllers_ArtistController_delete5_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logs/"), DynamicPart("username", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ArtistController_delete5_invoker = createInvoker(
    ArtistController_1.delete(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ArtistController",
      "delete",
      Seq(classOf[String]),
      "POST",
      this.prefix + """logs/""" + "$" + """username<[^/]+>""",
      """Delete Artist Account""",
      Seq()
    )
  )

  // @LINE:22
  private[this] lazy val controllers_ArtistController_edit6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logs/"), DynamicPart("username", """[^/]+""",true), StaticPart("/edit")))
  )
  private[this] lazy val controllers_ArtistController_edit6_invoker = createInvoker(
    ArtistController_1.edit(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ArtistController",
      "edit",
      Seq(classOf[String]),
      "GET",
      this.prefix + """logs/""" + "$" + """username<[^/]+>/edit""",
      """Edit Artist Account""",
      Seq()
    )
  )

  // @LINE:23
  private[this] lazy val controllers_ArtistController_update7_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logs/"), DynamicPart("username", """[^/]+""",true), StaticPart("/edit")))
  )
  private[this] lazy val controllers_ArtistController_update7_invoker = createInvoker(
    ArtistController_1.update(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ArtistController",
      "update",
      Seq(classOf[String]),
      "POST",
      this.prefix + """logs/""" + "$" + """username<[^/]+>/edit""",
      """""",
      Seq()
    )
  )

  // @LINE:26
  private[this] lazy val controllers_ArtistController_search8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("log")))
  )
  private[this] lazy val controllers_ArtistController_search8_invoker = createInvoker(
    ArtistController_1.search,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ArtistController",
      "search",
      Nil,
      "GET",
      this.prefix + """log""",
      """Search Artist Account""",
      Seq()
    )
  )

  // @LINE:27
  private[this] lazy val controllers_ArtistController_buscar9_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("log")))
  )
  private[this] lazy val controllers_ArtistController_buscar9_invoker = createInvoker(
    ArtistController_1.buscar,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ArtistController",
      "buscar",
      Nil,
      "POST",
      this.prefix + """log""",
      """""",
      Seq()
    )
  )

  // @LINE:30
  private[this] lazy val controllers_ArtistController_uploadSong10_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logs/"), DynamicPart("username", """[^/]+""",true), StaticPart("/uploadSong")))
  )
  private[this] lazy val controllers_ArtistController_uploadSong10_invoker = createInvoker(
    ArtistController_1.uploadSong(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ArtistController",
      "uploadSong",
      Seq(classOf[String]),
      "GET",
      this.prefix + """logs/""" + "$" + """username<[^/]+>/uploadSong""",
      """Update Song in a Artist Account""",
      Seq()
    )
  )

  // @LINE:31
  private[this] lazy val controllers_SongController_upload11_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logs/"), DynamicPart("username", """[^/]+""",true), StaticPart("/uploadSong")))
  )
  private[this] lazy val controllers_SongController_upload11_invoker = createInvoker(
    SongController_2.upload(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SongController",
      "upload",
      Seq(classOf[String]),
      "POST",
      this.prefix + """logs/""" + "$" + """username<[^/]+>/uploadSong""",
      """""",
      Seq()
    )
  )

  // @LINE:34
  private[this] lazy val controllers_SongController_delete12_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logs/"), DynamicPart("username", """[^/]+""",true), StaticPart("/"), DynamicPart("uri", """[^/]+""",true), StaticPart("/d")))
  )
  private[this] lazy val controllers_SongController_delete12_invoker = createInvoker(
    SongController_2.delete(fakeValue[String], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SongController",
      "delete",
      Seq(classOf[String], classOf[String]),
      "POST",
      this.prefix + """logs/""" + "$" + """username<[^/]+>/""" + "$" + """uri<[^/]+>/d""",
      """Delete Song""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_Assets_versioned0_route(params) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned0_invoker.call(Assets_0.versioned(path, file))
      }
  
    // @LINE:9
    case controllers_SongController_index1_route(params) =>
      call { 
        controllers_SongController_index1_invoker.call(SongController_2.index)
      }
  
    // @LINE:12
    case controllers_ArtistController_create2_route(params) =>
      call { 
        controllers_ArtistController_create2_invoker.call(ArtistController_1.create)
      }
  
    // @LINE:13
    case controllers_ArtistController_addArtist3_route(params) =>
      call { 
        controllers_ArtistController_addArtist3_invoker.call(ArtistController_1.addArtist)
      }
  
    // @LINE:16
    case controllers_ArtistController_show4_route(params) =>
      call(params.fromPath[String]("username", None)) { (username) =>
        controllers_ArtistController_show4_invoker.call(ArtistController_1.show(username))
      }
  
    // @LINE:19
    case controllers_ArtistController_delete5_route(params) =>
      call(params.fromPath[String]("username", None)) { (username) =>
        controllers_ArtistController_delete5_invoker.call(ArtistController_1.delete(username))
      }
  
    // @LINE:22
    case controllers_ArtistController_edit6_route(params) =>
      call(params.fromPath[String]("username", None)) { (username) =>
        controllers_ArtistController_edit6_invoker.call(ArtistController_1.edit(username))
      }
  
    // @LINE:23
    case controllers_ArtistController_update7_route(params) =>
      call(params.fromPath[String]("username", None)) { (username) =>
        controllers_ArtistController_update7_invoker.call(ArtistController_1.update(username))
      }
  
    // @LINE:26
    case controllers_ArtistController_search8_route(params) =>
      call { 
        controllers_ArtistController_search8_invoker.call(ArtistController_1.search)
      }
  
    // @LINE:27
    case controllers_ArtistController_buscar9_route(params) =>
      call { 
        controllers_ArtistController_buscar9_invoker.call(ArtistController_1.buscar)
      }
  
    // @LINE:30
    case controllers_ArtistController_uploadSong10_route(params) =>
      call(params.fromPath[String]("username", None)) { (username) =>
        controllers_ArtistController_uploadSong10_invoker.call(ArtistController_1.uploadSong(username))
      }
  
    // @LINE:31
    case controllers_SongController_upload11_route(params) =>
      call(params.fromPath[String]("username", None)) { (username) =>
        controllers_SongController_upload11_invoker.call(SongController_2.upload(username))
      }
  
    // @LINE:34
    case controllers_SongController_delete12_route(params) =>
      call(params.fromPath[String]("uri", None), params.fromPath[String]("username", None)) { (uri, username) =>
        controllers_SongController_delete12_invoker.call(SongController_2.delete(uri, username))
      }
  }
}
