
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/Asus/Documents/Cristian/Tesis/ncsong/conf/routes
// @DATE:Mon Jul 17 10:11:14 COT 2017

import play.api.mvc.Call


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers {

  // @LINE:9
  class ReverseSongController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:34
    def delete(uri:String, username:String): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "logs/" + implicitly[play.api.mvc.PathBindable[String]].unbind("username", play.core.routing.dynamicString(username)) + "/" + implicitly[play.api.mvc.PathBindable[String]].unbind("uri", play.core.routing.dynamicString(uri)) + "/d")
    }
  
    // @LINE:9
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
    // @LINE:31
    def upload(username:String): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "logs/" + implicitly[play.api.mvc.PathBindable[String]].unbind("username", play.core.routing.dynamicString(username)) + "/uploadSong")
    }
  
  }

  // @LINE:6
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:6
    def versioned(file:Asset): Call = {
      implicit val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public")))
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }

  // @LINE:12
  class ReverseArtistController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:16
    def show(username:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "logs/" + implicitly[play.api.mvc.PathBindable[String]].unbind("username", play.core.routing.dynamicString(username)))
    }
  
    // @LINE:13
    def addArtist(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "signin")
    }
  
    // @LINE:30
    def uploadSong(username:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "logs/" + implicitly[play.api.mvc.PathBindable[String]].unbind("username", play.core.routing.dynamicString(username)) + "/uploadSong")
    }
  
    // @LINE:22
    def edit(username:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "logs/" + implicitly[play.api.mvc.PathBindable[String]].unbind("username", play.core.routing.dynamicString(username)) + "/edit")
    }
  
    // @LINE:12
    def create(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "signin")
    }
  
    // @LINE:19
    def delete(username:String): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "logs/" + implicitly[play.api.mvc.PathBindable[String]].unbind("username", play.core.routing.dynamicString(username)))
    }
  
    // @LINE:26
    def search(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "log")
    }
  
    // @LINE:23
    def update(username:String): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "logs/" + implicitly[play.api.mvc.PathBindable[String]].unbind("username", play.core.routing.dynamicString(username)) + "/edit")
    }
  
    // @LINE:27
    def buscar(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "log")
    }
  
  }


}
