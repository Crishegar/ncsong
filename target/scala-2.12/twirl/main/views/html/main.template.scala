
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object main extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /*
 * This template is called from the `index` template. This template
 * handles the rendering of the page header and body tags. It takes
 * two arguments, a `String` for the title of the page and an `Html`
 * object to insert into the body of the page.
 */
  def apply/*7.2*/(title: String)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*7.32*/("""

"""),format.raw/*9.1*/("""<!DOCTYPE html>
<html lang="en">
    <head>
        """),format.raw/*12.62*/("""
        """),format.raw/*13.9*/("""<title>"""),_display_(/*13.17*/title),format.raw/*13.22*/("""</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" media="screen" href=""""),_display_(/*15.54*/routes/*15.60*/.Assets.versioned("stylesheets/main.css")),format.raw/*15.101*/("""">
        <link rel="shortcut icon" type="image/png" href=""""),_display_(/*16.59*/routes/*16.65*/.Assets.versioned("images/favicon.png")),format.raw/*16.104*/("""">
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" media="screen,projection" href=""""),_display_(/*20.81*/routes/*20.87*/.Assets.versioned("stylesheets/materialize.min.css")),format.raw/*20.139*/(""""/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body bgcolor="#ce93d8">
    <h2>NCSongs</h2>
    <nav>
        <div class="nav-wrapper teal darken-4">
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="""),_display_(/*30.30*/controllers/*30.41*/.routes.SongController.index),format.raw/*30.69*/(""">Home</a></li>
                <li><a href="""),_display_(/*31.30*/controllers/*31.41*/.routes.ArtistController.create),format.raw/*31.72*/(""">Sing in</a></li>
                <li><a href="""),_display_(/*32.30*/controllers/*32.41*/.routes.ArtistController.search),format.raw/*32.72*/(""">Search Account</a></li>
            </ul>
        </div>
    </nav>


    """),format.raw/*39.32*/("""
        """),_display_(/*40.10*/content),format.raw/*40.17*/("""


        """),format.raw/*43.9*/("""<script src=""""),_display_(/*43.23*/routes/*43.29*/.Assets.versioned("javascripts/main.js")),format.raw/*43.69*/("""" type="text/javascript"></script>
         <!--Import jQuery before materialize.js-->
         <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src=""""),_display_(/*46.46*/routes/*46.52*/.Assets.versioned("javascripts/materialize.min.js")),format.raw/*46.103*/(""""></script>
    </body>
</html>
"""))
      }
    }
  }

  def render(title:String,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title)(content)

  def f:((String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title) => (content) => apply(title)(content)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Jul 17 10:21:58 COT 2017
                  SOURCE: C:/Users/Asus/Documents/Cristian/Tesis/ncsong/app/views/main.scala.html
                  HASH: 2bec1d650449fa9ffaa76b09cf1facdbc230d565
                  MATRIX: 1211->266|1336->296|1366->300|1449->408|1486->418|1521->426|1547->431|1733->590|1748->596|1811->637|1900->699|1915->705|1976->744|2262->1003|2277->1009|2351->1061|2754->1437|2774->1448|2823->1476|2895->1521|2915->1532|2967->1563|3042->1611|3062->1622|3114->1653|3223->1825|3261->1836|3289->1843|3330->1857|3371->1871|3386->1877|3447->1917|3709->2152|3724->2158|3797->2209
                  LINES: 33->7|38->7|40->9|43->12|44->13|44->13|44->13|46->15|46->15|46->15|47->16|47->16|47->16|51->20|51->20|51->20|61->30|61->30|61->30|62->31|62->31|62->31|63->32|63->32|63->32|69->39|70->40|70->40|73->43|73->43|73->43|73->43|76->46|76->46|76->46
                  -- GENERATED --
              */
          